local addon, dark_addon = ...
local disp = LibStub("LibDispellable-1.0")
local UnitReverseDebuff = dark_addon.environment.unit_reverse_debuff

local group = { }

local function check_removable(removable_type)
  for unit in dark_addon.environment.iterator() do
    local debuff, count, duration, expires, caster, found_debuff = UnitReverseDebuff(unit.unitID, dark_addon.data.removables[removable_type])
    if debuff and (count == 0 or count >= found_debuff.count) and unit.health.percent <= found_debuff.health then
      return unit
    end
  end
  return false
end

local function group_removable(...)
  for i = 1, select('#', ...) do
    local removable_type, _ = select(i, ...)
    if dark_addon.data.removables[removable_type] then
      local possible_unit = check_removable(removable_type)
      if possible_unit then
        return possible_unit
      end
    end
  end
  return false
end

function group:removable(...)
  return group_removable
end

local function group_dispellable(spell)
  for unit in dark_addon.environment.iterator() do
    if disp:CanDispelWith(unit.unitID, spell) then 
      return unit
    end
  end
  return false
end

function group:dispellable(spell)
  return group_dispellable
end

function group_under(...)
  local percent, distance, effective = ...
  local count = 0
  for unit in dark_addon.environment.iterator() do
    if unit.alive
      and (
        (distance and unit.distance <= distance)
        or not distance
      )
      and (
        (effective and unit.health.effective < percent) 
        or (not effective and unit.health.percent < percent)
      ) then
      count = count + 1
    end
  end
  return count
end

function group:under(...)
  return group_under
end

function dark_addon.environment.conditions.group()
  return setmetatable({ }, {
    __index = function(t, k)
      return group[k](t)
    end
  })
end
